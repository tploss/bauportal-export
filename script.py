#!/usr/bin/env python3
import os
import pathlib
import re
from bs4 import BeautifulSoup
#from pprint import pprint
import requests

from tqdm import tqdm

import magic

# Function to get MIME type from content
m = magic.Magic(extension=True)
def get_content_type_extension(content):
    return m.from_buffer(content)

def read_html_table_from_file(file_path):
    # Open the local HTML file
    with open(file_path, 'r', encoding='utf-8') as file:
        html_content = file.read()
        soup = BeautifulSoup(html_content, 'html.parser') # parse

        # Find the HTML table in the page
        table = soup.find('table')

        # Check if a table was found
        if not table:
            raise ValueError("No table found in the HTML file.")
        
        data = []
        for row in table.find_all('tr'):
            row_data = []
            i = 0
            # Iterate through each cell in the row
            for cell in row.find_all(['td']):
                i += 1
                if i == 2:
                    # get the text of the tag
                    row_data.append(cell.get_text(strip=True))
                if i == 5:
                    # get the 'herunterladen' link
                    a = cell.find('a')
                    if a: 
                        row_data.append(a.get('href'))
            if len(row_data) == 2:
                data.append(row_data)
        
        translation_table = {ord('.'): '', ord(' '): '_', ord('/'): '-', ord(','): '', ord('('): '', ord(')'): '', ord('ä'): 'ae', ord('ü'): 'ue', ord('ö'): 'oe', ord('ß'): 'ss'}
        for row in data:
            # transform number.number to number-number to take care of dates
            pattern = re.compile(r'([0-9])\.([0-9])')
            # Define a callback function to replace dots with hyphens
            def replace_dots(match):
                return f'{match.group(1)}-{match.group(2)}'
            # Substitute dots with hyphens in all occurrences using the callback function
            row[0] = pattern.sub(replace_dots, row[0])
        
            # translate characters that should not be used with file names
            row[0] = str(row[0]).strip().lower().translate(translation_table)
            # compress spaces
            row[0] = ' '.join(row[0].split())
            
        return data

# Replace 'your_file_path_here' with the path to your local HTML file
file_path = 'table.html'
d = read_html_table_from_file(file_path)

# deal with name duplications
d = sorted(d, key=lambda x: x[0])
for i, row in enumerate(d):
    prev = i - 1
    while prev >= 0 and str(d[prev][0]).startswith(row[0]):
        prev -= 1
    if prev != i -1:
        row[0] += f"_{i - prev}"

# pretty print the data structure
# pprint(d)

export_dir = "exports"
pathlib.Path(export_dir).mkdir(parents=True, exist_ok=True)

base_url = "https://bauportal.lra-ebe.de/"
# Replace this with the actual cookies from your browser
cookies = {"PROSOZBau": os.environ.get('COOKIE', '')}
session = requests.Session()
session.cookies.update(cookies)
for row in tqdm(d):
    url = base_url + row[1]
    response = session.get(url)

    # Process the response
    if response.status_code == 200:
        row[0] += f".{get_content_type_extension(response.content)}"

        with open(f"{export_dir}/{row[0]}", "wb") as file:
            file.write(response.content)
    else:
        print(f"Failed to make the request for {row[0]}. Status code: {response.status_code}")
