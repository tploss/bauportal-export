# bauportal downloader

Downloads all files from the EBE bauportal "Mediencenter".

## Input

Requires the following data:
1. The html table shown on the website. 
Inspect the table, copy its html code and paste it into a file called `table.html`.
![Screenshot showing how to get the html table](./images/table.png)
1. The auth cookie used for downloads.
Check the network logs and copy the request cookie used when communicating with the bauportal website.
Should be a 32 character hexadecimal number, e.g. `8f2b1a7c9e0d3f6a5c7b8a9d2f3e4b6c`.
Set it as env variable `COOKIE`.
![Screenshot showing how to get the cookie](./images/cookie.png)

## Output

Downloads all files into an `exports` folder.
Replaces non filesystem friendly characters.

## Exectution

If you have prepared the input (`table.html` file and `COOKIE` env variable run the following command):
```bash
python3 -m venv .venv && source .venv/bin/activate && pip install --upgrade pip && pip install -r requirements.txt && ./script.py
```

